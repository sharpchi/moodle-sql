/**
 * List courses that are using the self-enrolment enrolment method.
 */
SELECT e.courseid,c.shortname, COUNT(distinct(ue.id)) AS users FROM mdl_enrol e
JOIN mdl_user_enrolments ue ON e.id=ue.enrolid
JOIN mdl_course c ON e.courseid=c.id
WHERE e.enrol='self'
GROUP BY e.courseid;
